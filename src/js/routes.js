'use strict';

/**
 * Route configuration for the RDash module.
 */
 angular.module('RDash').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

         // For unmatched routes
        $urlRouterProvider.otherwise('/overview');

        // Application routes
        $stateProvider
            .state('overview', {
                url: '/overview',
                templateUrl: 'templates/dashboard.html'
            })
            .state('ratingfactor', {
                url: '/ratingfactor',
                templateUrl: 'templates/ratingfactor.html'
            })
            .state('impact', {
                url: '/impact',
                templateUrl: 'templates/impact.html'
            });
    }
    ]);