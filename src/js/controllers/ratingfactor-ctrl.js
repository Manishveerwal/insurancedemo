
app.controller('RatingFactorCtrl', ['$scope', '$compile', '$http', '$timeout', RatingFactorCtrl]);

function RatingFactorCtrl($scope, $compile, $http, $timeout) {

  $scope.rowCollection = [ {"firstHeaderRow": "Base", "currentDataColumn2": "100", "newDataColumn2": "120","currentDataColumn3": "80", "newDataColumn3": "90",
"currentDataColumn4": "40", "newDataColumn4": "60", "currentDataColumn5": "40", "newDataColumn5": "55"},
{"firstHeaderRow": "Car Year <2000", "currentDataColumn2": "1.7", "newDataColumn2": "1.6","currentDataColumn3": "1.3", "newDataColumn3": "1.4",
"currentDataColumn4": "0.8", "newDataColumn4": "1.0", "currentDataColumn5": "1.3", "newDataColumn5": "1.25"},
{"firstHeaderRow": "Car Year 2001-2008", "currentDataColumn2": "1.65", "newDataColumn2": "1.6","currentDataColumn3": "1.25", "newDataColumn3": "1.4",
"currentDataColumn4": "1.8", "newDataColumn4": "1.0", "currentDataColumn5": "1.10", "newDataColumn5": "1.2"},
{"firstHeaderRow": "Car Year 2009-2016", "currentDataColumn2": "1.34", "newDataColumn2": "1.4","currentDataColumn3": "1.35", "newDataColumn3": "1.2",
"currentDataColumn4": "0.9", "newDataColumn4": "1.4", "currentDataColumn5": "1.1", "newDataColumn5": "1.2"},
{"firstHeaderRow": "Driver Age 16-18", "currentDataColumn2": "0.8", "newDataColumn2": "0.9","currentDataColumn3": "1.1", "newDataColumn3": "1.15",
"currentDataColumn4": "0.85", "newDataColumn4": "0.9", "currentDataColumn5": "0.9", "newDataColumn5": "1.0"},
{"firstHeaderRow": "Driver Age 19-25", "currentDataColumn2": "0.8", "newDataColumn2": "0.9","currentDataColumn3": "1.1", "newDataColumn3": "1.15",
"currentDataColumn4": "0.85", "newDataColumn4": "0.9", "currentDataColumn5": "0.9", "newDataColumn5": "1.0"},
{"firstHeaderRow": "Driver Age 25-35", "currentDataColumn2": "0.8", "newDataColumn2": "0.9","currentDataColumn3": "1.1", "newDataColumn3": "1.15",
"currentDataColumn4": "0.66", "newDataColumn4": "0.7", "currentDataColumn5": "0.8", "newDataColumn5": "0.9"}
    ];

}