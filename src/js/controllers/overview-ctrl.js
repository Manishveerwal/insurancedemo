/**
 * Alerts Controller
 */

 app.controller('OverviewCtrl', ['$scope', 'DataService', '$timeout', OverviewCtrl]);

 function OverviewCtrl($scope, DataService, $timeout) {

    $scope.states = ["Select State","Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida",
    "Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts",
    "Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","NewHampshire","NewJersey","NewMexico",
    "NewYork","NorthCarolina","NorthDakota","Ohio","Oklahoma","Oregon","Pennsylvania","RhodeIsland","SouthCarolina",
    "SouthDakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","WestVirginia","Wisconsin","Wyoming"];

    $scope.dates = ["Select Date","01/01/2016","02/01/2016","03/01/2016","04/01/2016","05/01/2016","06/01/2016",
    "07/01/2016","08/01/2016","09/01/2016","10/01/2016","11/01/2016","12/01/2016","01/01/2017",
    "02/01/2017","03/01/2017","04/01/2017","05/01/2017","06/01/2017","07/01/2017","08/01/2017",
    "09/01/2017","10/01/2017","11/01/2017","12/01/2017"];

    $scope.gridsterOptions = {
        minRows: 2,
        maxRows: 100,
        margins: [10, 10],
        columns:6,
        mobileModeEnabled: false,
        draggable: {
            enabled: false,
            handle: 'h3'
        },
        resizable: {
           enabled: false,

     // optional callback fired when resize is started
     start: function(event, $element, widget) {},
     
     // optional callback fired when item is resized,
     resize: function(event, $element, widget) {
         if (widget.chart.api) widget.chart.api.update();
     }, 

      // optional callback fired when item is finished resizing 
      stop: function(event, $element, widget) {
         $timeout(function(){
           if (widget.chart.api) widget.chart.api.update();
       },400)
     } 
 },
};

$scope.dashboard = {
    widgets: [{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Average Premium: $546",
        chart: {
          options: DataService.discreteBarChart.options(),
          data: DataService.discreteBarChart.data(),
          api: {} 
      }
  },{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Average Tenure: 8 Months",
        chart: {
          options: DataService.discreteBarChart2.options(),
          data: DataService.discreteBarChart2.data(),
          api: {} 
      }
  },{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Average Vehicles Covered: 1.3M",
        chart: {
          options: DataService.discreteBarChart3.options(),
          data: DataService.discreteBarChart3.data(),
          api: {} 
      }
  },{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Average Premium: $546",
        chart: {
          options: DataService.discreteBarChart4.options(),
          data: DataService.discreteBarChart4.data(),
          api: {} 
      }
  }]
};

$scope.config = {
    visible: false
};
$timeout(function(){
    $scope.config.visible = true;
}, 200);
}