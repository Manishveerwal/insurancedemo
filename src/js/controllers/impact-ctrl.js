/**
 * Alerts Controller
 */

 app.controller('ImpactCtrl', ['$scope', 'DataService', '$timeout', ImpactCtrl]);

 function ImpactCtrl($scope, DataService, $timeout) {
    $scope.gridsterOptions = {
        minRows: 2,
        maxRows: 100,
        margins: [10, 10],
        columns:6,
        mobileModeEnabled: false,
        draggable: {
            enabled: false,
            handle: 'h3'
        },
        resizable: {
           enabled: false,

     // optional callback fired when resize is started
     start: function(event, $element, widget) {},
     
     // optional callback fired when item is resized,
     resize: function(event, $element, widget) {
         if (widget.chart.api) widget.chart.api.update();
     }, 

      // optional callback fired when item is finished resizing 
      stop: function(event, $element, widget) {
         $timeout(function(){
           if (widget.chart.api) widget.chart.api.update();
       },400)
     } 
 },
};

$scope.graphData= [
  {
    "Month": "Jan-11",
    "storeId": 1,
    "Sales": 14
  },{
    "Month": "Feb-11",
    "storeId": 1,
    "Sales": 14
  },{
    "Month": "March-11",
    "storeId": 1,
    "Sales": 17
  },{
    "Month": "Jan-11",
    "storeId": 2,
    "Sales": 14
  },{
    "Month": "Feb-11",
    "storeId": 2,
    "Sales": 16
  },{
    "Month": "March-11",
    "storeId": 2,
    "Sales": 8
  }
];

$scope.dashboard = {
    widgets: [{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Number of Policies",
        chart: {
          options: DataService.lineChart.options(),
          data: DataService.lineChart.data(),
          api: {} 
      }
  },{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Gross Written Premium",
        chart: {
          options: DataService.lineChart2.options(),
          data: DataService.lineChart2.data(),
          api: {} 
      }
  },{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Loss Ratio",
        chart: {
          options: DataService.lineChart3.options(),
          data: DataService.lineChart3.data(),
          api: {} 
      }
  },{
        col: 0,
        row: 0,
        sizeY: 1,
        sizeX: 2,
        name: "Combined Ratio",
        chart: {
          options: DataService.lineChart.options(),
          data: DataService.lineChart.data(),
          api: {} 
      }
  }]
};

$scope.config = {
    visible: false
};
$timeout(function(){
    $scope.config.visible = true;
}, 200);
}