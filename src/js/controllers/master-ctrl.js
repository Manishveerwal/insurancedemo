/**
 * Master Controller
 */

app.controller('MasterCtrl', ['$scope', '$cookieStore', '$location', MasterCtrl]);

function MasterCtrl($scope, $cookieStore, $location) {

    $scope.states = ["Select State","Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida",
    "Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts",
    "Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","NewHampshire","NewJersey","NewMexico",
    "NewYork","NorthCarolina","NorthDakota","Ohio","Oklahoma","Oregon","Pennsylvania","RhodeIsland","SouthCarolina",
    "SouthDakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","WestVirginia","Wisconsin","Wyoming"];

    $scope.dates = ["Select Date","01/01/2016","02/01/2016","03/01/2016","04/01/2016","05/01/2016","06/01/2016",
    "07/01/2016","08/01/2016","09/01/2016","10/01/2016","11/01/2016","12/01/2016","01/01/2017",
    "02/01/2017","03/01/2017","04/01/2017","05/01/2017","06/01/2017","07/01/2017","08/01/2017",
    "09/01/2017","10/01/2017","11/01/2017","12/01/2017"];
    
    var mobileView = 992;

    $scope.getWidth = function() {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function(newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = ! $cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function() {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };

    window.onresize = function() {
        $scope.$apply();
    };
}